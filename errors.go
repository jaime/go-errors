package errors

import "errors"

// ErrSample is a public error sample
var ErrSample = errors.New("this is an error")

// DoSomething returns an ErrSample
func DoSomething(err error) error {
	if err != nil {
		return err
	}
	return ErrSample
}
