package main

import (
	stderrors "errors"
	"fmt"
	"reflect"

	"gitlab.com/jaime/go-errors"
)

func main() {
	err1 := errors.DoSomething(nil)
	fmt.Printf("type of err1: %q - err1 == errors.ErrSample? '%t'\n", reflect.TypeOf(err1), err1 == errors.ErrSample)
	err2 := errors.DoSomething(stderrors.New("another error"))
	fmt.Printf("type of err2: %q - err2 == errors.ErrSample? '%t'\n", reflect.TypeOf(err2), err2 == errors.ErrSample)

}
